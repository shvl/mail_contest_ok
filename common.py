import math
import numpy as np
import os
from PIL import Image, ImageDraw, ImageFont

TRAIN_COLOR = (200, 0, 0)
ANSWER_COLOR = (0, 200, 0)
if os.environ.get('MAC', 'False') == 'True':
    FONT_PATH = '/Users/aliaksandrshavel/anaconda3/lib/python3.7/site-packages/matplotlib/mpl-data/fonts/ttf/DejaVuSans.ttf'
else:
    FONT_PATH = '/usr/local/lib/python3.5/dist-packages/matplotlib/mpl-data/fonts/ttf/DejaVuSans.ttf'
FONT = ImageFont.truetype(FONT_PATH, size=8)
IMAGE_PADDING = 10

def add_random_factor(train, randomize=0):
    res = []
    for i in train:
        offset = 0
        if randomize > 0:
            offset = np.random.randint(-randomize, randomize)
        res.append(i + offset)
        
    return res

add_random_factor([1,2,3,4], 0)
add_random_factor([1,2,3,4], 1)

def get_random_sample_as_list(samples):
    count = samples.shape[0]
    random_index = np.random.randint(count)
    return samples[random_index].reshape(6).tolist()[2:]

samples_test = [
    [1,2,3,4,5,6],
    [2,2,4,4,5,6],
    [3,2,5,4,5,6],
]
samples_test_answers = [
    [10,20,30,40,50],
]

def handle_row_random_batch(train, answer, samples_count=6, premutations=5, randomize=5):
    a = np.asarray(train)
    answer_array = np.asarray(answer)
    count = train.shape[0]
    rows = []
    for _ in range(premutations):
        row = []
        row.append(answer_array[0][0])
        for i in range(samples_count):
            random_sample = get_random_sample_as_list(a)
            random_sample = add_random_factor(random_sample, randomize)
            width = random_sample[2] - random_sample[0]
            height = random_sample[3] - random_sample[1]
            random_sample.append(width)
            random_sample.append(height)
            if min(width, height) != 0:
                random_sample.append(max(width, height) / min(width, height))
            else:
                random_sample.append(0)
            row = row + random_sample
        row = row + answer_array.reshape(5).tolist()[1:]
        rows.append(row)
    return rows

def handle_row_random_batch_offset(train, answer, samples_count=6, premutations=5, randomize=5):
    a = np.asarray(train)
    answer_array = np.asarray(answer)
    count = train.shape[0]
    rows = []
    for _ in range(premutations):
        row = []
        row.append(answer_array[0][0])
        for i in range(samples_count):
            random_sample = get_random_sample_as_list(a)
            random_sample = add_random_factor(random_sample, randomize)
            width = random_sample[2] - random_sample[0]
            height = random_sample[3] - random_sample[1]
            random_sample.append(width)
            random_sample.append(height)
            if min(width, height) != 0:
                random_sample.append(max(width, height) / min(width, height))
            else:
                random_sample.append(0)
            row = row + random_sample
        row = row + answer_array.reshape(7).tolist()[1:]
        rows.append(row)
    return rows

def get_data(item_id, train_data, answers_data=None):
    train = train_data.loc[train_data.itemId == item_id]
    answer = answers_data[answers_data.itemId == item_id]
    return train, answer

def draw_rectangle(draw, x0, y0, x1, y1, text, color):
    draw.rectangle((x0, y0, x1, y1), outline=color)
    text = "{} ({}, {}) ({}, {})".format(text, x0, y0, x1, y1)
    draw.text((x0 + 2, y0 - 12), text, font=FONT, fill=color)
    
def handle_row_min_avg(train, answer):
    train_array = np.asarray(train)
    answer_array = np.asarray(answer)
    row = []
    row.append(answer_array[0][0])
    for i in range(4):
        if i < 2:
            row.append( np.min(train_array[:,i+2]) )
        else:
            row.append( np.max(train_array[:,i+2]) )
            
    row.append( row[3] - row[1] )
    row.append( row[4] - row[2] )
    row.append( (row[3] - row[1]) * (row[4] - row[2])  )
        
    
#     for i in range(4):
#         if i < 2:
#             row.append( np.max(train_array[:,i+2]) )
#         else:
#             row.append( np.min(train_array[:,i+2]) )
#     for i in range(4):
#         row.append( np.average(train_array[:,i+2]) )

    row = row + answer_array.reshape(5).tolist()[1:]
    return [row]

def handle_row_by_batch(train, answer, batch_size=6):
    train_array = np.asarray(train)
    answer_array = np.asarray(answer)
    count = train.shape[0]
    rows = []
    for i in range(math.ceil(count / batch_size)):
        end = min((i + 1) * batch_size, count)
        row = train_array[i * batch_size: end, 2:]
        row = np.concatenate(row)
        if end % batch_size > 0:
            row = np.pad(row, (0,(batch_size - end % batch_size) * 4), 'constant')
        rows.append([answer_array[0][0]] + row.tolist() + answer_array.reshape(5).tolist()[1:])
        

    return rows

