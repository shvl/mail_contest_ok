import numpy as np
from PIL import Image, ImageDraw, ImageFont
from common import get_data, handle_row_min_avg
import os


TRAIN_COLOR = (200, 0, 0)
ANSWER_COLOR = (0, 200, 0)
PREDICT_COLOR = (0, 0, 200)
MAX_COLOR = (0, 200, 200)
if os.environ.get('MAC', 'False') == 'True':
    FONT_PATH = '/Users/aliaksandrshavel/anaconda3/lib/python3.7/site-packages/matplotlib/mpl-data/fonts/ttf/DejaVuSans.ttf'
else:
    FONT_PATH = '/usr/local/lib/python3.5/dist-packages/matplotlib/mpl-data/fonts/ttf/DejaVuSans.ttf'
FONT = ImageFont.truetype(FONT_PATH, size=8)
IMAGE_PADDING = 10


def get_bounds(train, answer):
    x = np.max(np.concatenate((
        np.asarray(train.Xmax), 
        np.asarray(train.Xmin),
        np.asarray(answer.Xmax_true), 
        np.asarray(answer.Xmin_true),
    )))
    
    y = np.max(np.concatenate((
        np.asarray(train.Ymax), 
        np.asarray(train.Ymin),
        np.asarray(answer.Ymax_true), 
        np.asarray(answer.Ymin_true),
    )))
    return np.max(x), np.max(y), np.min(x), np.min(y)

def draw_rectangle(draw, x0, y0, x1, y1, text, color):
    draw.rectangle((x0, y0, x1, y1), outline=color)
    text = "{} ({}, {}) ({}, {})".format(text, x0, y0, x1, y1)
    draw.text((x0 + 2, y0 - 12), text, font=FONT, fill=color)

def draw_rectangles(item_id, train_data, answers_data):
    train, answer = get_data(item_id, train_data, answers_data)
    max_x, max_y, min_x, min_y =  get_bounds(train, answer)
    x1,y1,x2,y2 = handle_row_min_avg(train, answer)[0][1:5]
    ax1,ay1,ax2,ay2 = handle_row_min_avg(train, answer)[0][-4:]
    print(handle_row_min_avg(train, answer)[0])
    print(x1,y1,x2,y2)
    
    img = Image.new('RGB', (max_x + IMAGE_PADDING, max_y + IMAGE_PADDING))
    draw = ImageDraw.Draw(img)
    for row in train.iterrows():
        draw_rectangle(
            draw, 
            row[1]['Xmin'], 
            row[1]['Ymin'],
            row[1]['Xmax'], 
            row[1]['Ymax'], 
            'user: ' + str(row[1]['userId']), 
            TRAIN_COLOR
        )
    
#     for row in answer.iterrows():
#         draw_rectangle(
#             draw, 
#             row[1]['Xmin_true'], 
#             row[1]['Ymin_true'],
#             row[1]['Xmax_true'], 
#             row[1]['Ymax_true'], 
#             'answer', 
#             ANSWER_COLOR
#         )
    
    draw_rectangle( draw, x1,y1,x2,y2, 'max', MAX_COLOR)
    draw_rectangle( draw, ax1,ay1,ax2,ay2, 'answer', ANSWER_COLOR)
#     draw_rectangle( draw, x3,y3,x4,y4, 'avg', MAX_COLOR)
#     img.save(str(item_id)+'.png')
        
    return img
